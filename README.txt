
Tiered Taxonomy Block
---------------------
Tiered taxonomy block module takes 2 vocabularies and creates a tiered block.
1st tier is a list of all 1st vocabulary terms. Under each 1st vocabulary term,
a list of 2nd vocabulary terms are displayed only if there are published nodes
that are tagged with both 1st and 2nd vocabulary terms.

Installation
-------------
Place the module folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the Tiered taxonomy block module.

Configuration
-------------
Go to Administer -> Site configuration -> Tiered taxonomy block and pick a parent and child vocabularies.
Then go to Administer -> Site building -> Blocks and enable Tiered taxonomy block.
